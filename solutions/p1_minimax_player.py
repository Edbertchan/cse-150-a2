# -*- coding: utf-8 -*-
__author__ = 'Sivasubramanian Chandrasegarampillai, Walter Curnow'
__email__ = 'rchandra@uci.edu,wcurnow@uci.edu'

from assignment2 import Player, State, Action

class MinimaxPlayer(Player):
    def move(self, state):
        """Calculates the best move from the given board using the minimax algorithm.

        Args:
            state (State): The current state of the board.

        Returns:
            the next move (Action)
        """
        #We implement as in page 164 of the book. This means we need to redefine the Minimax Function
        utility_value = (self.minimax(state, 0))[1]
        #position = 0
        #i=0
        #for action in state.actions():
        	#if(state.result(action) == utility_value):
        		#position = i
        		#break
        	#i += 1
        #end
        #print utility_value
        # TODO implement this
        return (state.actions())[utility_value]

    def minimax(self,state,depth):
    	"""Returns the position of the next state of the minimax as a tuple (utility, position)
    	"""
    	#we assume first that we are not ourself. We do this by depth
    	self_player = False

        #since the values are either 1,0, or 1, we just need something outside that range when
        #itterating through the children
    	utility_store = 2
    	position = 0
    	i = 0
    	if (depth % 2  == 0):
    		self_player = True
    		utility_store = -2

    	prev_utility_store = utility_store

    	#terminal test. We arrive here on win or draw for the player who last played
    	if (state.is_terminal()):
    		#return our utility value of our current player. This is determined by depth.
    		# we need to send this utility back up. We use -1 to denote that this position is
    		#terminated and that there are no valid moves
    		return (state.utility(self),0)
    	#if we can still make a move, let's make it. If we are ourself, we want to max.
    	#if we are not ourselves, we want the min
        for action in state.actions():
        	possible_state = state.result(action)
        	if (self_player):
        		#calculate max
        		utility_store = max(utility_store, self.minimax(possible_state, depth+1)[0])
        	else:
        		#calculate min
        		utility_store = min(utility_store, self.minimax(possible_state, depth+1)[0])
        	if (utility_store != prev_utility_store):
        		position = i
        		prev_utility_store = utility_store
        	i += 1

        return (utility_store, position)



