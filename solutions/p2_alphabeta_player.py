# -*- coding: utf-8 -*-
__author__ = 'Sivasubramanian Chandrasegarampillai, Walter Curnow'
__email__ = 'rchandra@uci.edu,wcurnow@uci.edu'

from assignment2 import Player, State, Action

class AlphaBetaPlayer(Player):

    boards_visited_min = {} # List of previously visited boards  

    boards_visited_max = {} # List of previously visited boards 

    def move(self, state):
        """Calculates the best move from the given board using the minimax algorithm with alpha-beta pruning.

        Args:
            state (State): The current state of the board.

        Returns:
            the next move (Action)
        """
        #from pg 170
        position = (self.max_value(state, -99.0,99.0,True))[1]

        #print (state.actions())[position]
        self.boards_visited_max.clear()
        self.boards_visited_min.clear()
        return (state.actions())[position]

    def min_value(self,state,alpha,beta,display):
      #if we've visited this board before, we just return because there is no need
      #to continue
      if (state.is_terminal()):
        return (state.utility(self),0)

      if(self.boards_visited_min.has_key(str(state))):
        return (-99.0,0)
      self.boards_visited_min[str(state)] = 1
   			
   		
      #we continue if we have not visited this board
      
      v = 99.0
      position = 0
      previous_v = v
      i=0
      for action in state.actions():
        v = min(v, (self.max_value(state.result(action),alpha,beta,False))[0])
        if (v <= alpha):
          return (v,i)
   				#return (v,position)
        if( v != previous_v):
          position = i
          previous_v = v
        beta = min(beta, v)
        i += 1
      return (v,position)

    def max_value(self,state,alpha,beta,display):
      if (state.is_terminal()):
    		return (state.utility(self), 0)

      if(self.boards_visited_max.has_key(str(state))):
        return (99.0,0)
      #we continue if we have not visited this board
      self.boards_visited_max[str(state)] = 1
      v = -99.0
      position = 0
      previous_v = v
      i=0
      for action in state.actions():
        v = max(v, (self.min_value(state.result(action),alpha,beta,False))[0])


        if (v >= beta):
          return(v,i)

        if(v != previous_v):
   				position = i
   				previous_v = v
        		#return (v,position)

        alpha = max(alpha, v)
        i += 1

      return (v,position)

   	


