# -*- coding: utf-8 -*-
__author__ = 'Sivasubramanian Chandrasegarampillai, Walter Curnow'
__email__ = 'rchandra@uci.edu,wcurnow@uci.edu'

import heapq

from assignment2 import Player


class EvaluationPlayer(Player):
    def move(self, state):
        """Calculates the best move after 1-ply look-ahead with a simple evaluation function.

        Args:
            state (State): The current state of the board.

        Returns:
            the next move (Action)
        """

        # *You do not need to modify this method.*
        best_move = None
        max_value = -1.0
        my_color = state.to_play.color

        for action in state.actions():
            if self.is_time_up():
                break

            result_state = state.result(action)
            value = self.evaluate(result_state, my_color)
            if value > max_value:
                max_value = value
                best_move = action

        # Return the move with the highest evaluation value
        return best_move

    def has_streak(self, state, color, streak, li, lj):
        """Determines if a state has a streak of the specified length at the specified li,lj coordinates

            Args:
                state (State): The state instance of the current board
                color (int): The color to test for
                streak (int): The length of the streak to test for
                li,lj (ints): The coordinates of the board space to test for

            Returns:
                True if there is a streak with the specified length with the li,lj space inside it.
                False otherwise.
        """
        
        # The coordinates where we want to start the checks
        t = max(li - streak + 1, 0)
        b = min(state.M - streak, li)
        l = max(lj - streak + 1, 0)
        r = min(state.N - streak, lj)

        # Check vertical
        for i in range(t, b + 1):
            if all((state.board[i + k][lj] == color for k in range(streak))):
                return True

        # Check horizontal
        for j in range(l, r + 1):
            if all((state.board[li][j + k] == color for k in range(streak))):
                return True

        # Check top left to bottom right diagonal
        for d in range(-min(li - t, lj - l), min(b - li, r - lj) + 1):
            if all((state.board[li + d + k][lj + d + k] == color for k in range(streak))):
                return True

        # Check bottom left to top right diagonal
        for d in range(-min(state.M - 1 - li, lj, streak - 1), min(li - streak + 1, state.N - streak - lj, 0) + 1):
            if all((state.board[li - d - k][lj + d + k] == color for k in range(streak))):
                return True
            
        return False

    def evaluate(self, state, color):
        """Evaluates the state for the player with the given stone color.

        This function calculates the length of the longest ``streak'' on the board
        (of the given stone color) divided by K.  Since the longest streak you can
        achieve is K, the value returned will be in range [1 / state.K, 1.0].

        Args:
            state (State): The state instance for the current board.
            color (int): The color of the stone for which to calculate the streaks.

        Returns:
            the evaluation value (float), from 1.0 / state.K (worst) to 1.0 (win).
        """

        # TODO implement this
        ans = 0
        
        for y in range(state.M): # Goes through each board space and checks
            for x in range(state.N): # the heuristic if the space contains
                if(state.board[y][x] == color): # a piece of the given color
                    streak = 1
                    while streak+1 <= state.K and self.has_streak(state, color, streak+1, y, x):
                        streak = streak+1
                    ans = max(ans, streak)
                    
        return float(ans) / float(state.K)
