# -*- coding: utf-8 -*-
__author__ = 'Sivasubramanian Chandrasegarampillai, Walter Curnow'
__email__ = 'rchandra@uci.edu,wcurnow@uci.edu'

from assignment2 import Player, State, Action


class YourCustomPlayer(Player):
    boards_visited_min = {} # List of previously visited boards
    boards_visited_max = {} # List of previously visited boards
    
    @property
    def name(self):
        """Returns the name of this agent. Try to make it unique!"""
        return 'Drunk Shortsighted Bobby'

    def move(self, state):
        """Calculates the absolute best move from the given board position using magic.
        
        Args:
            state (State): The current state of the board.

        Returns:
            your next Action instance
        """
        self.isThinking = True
        my_move = state.actions()[0]
        self.limit = 2

        while not self.is_time_up() and self.feel_like_thinking():
            # Do some thinking here
            my_move = self.do_the_magic(state)
            self.limit += 1

        # Time's up, return your move
        # You should only do a small amount of work here, less than one second.
        # Otherwise a random move will be played!
        return my_move

    def feel_like_thinking(self):
        # You can code here how long you want to think perhaps.
        return self.isThinking

    def do_the_magic(self, state):
        # Do the magic, return the first available move!
        
        position = (self.max_value(state,-99.0,99.0,True,0))[1]
        
        self.boards_visited_max.clear()
        self.boards_visited_min.clear()
        self.isThinking = False
        
        return state.actions()[position]

    def has_streak(self, state, color, streak, li, lj):
        """Determines if a state has a streak of the specified length at the specified li,lj coordinates

            Args:
                state (State): The state instance of the current board
                color (int): The color to test for
                streak (int): The length of the streak to test for
                li,lj (ints): The coordinates of the board space to test for

            Returns:
                True if there is a streak with the specified length with the li,lj space inside it.
                False otherwise.
        """
        
        # The coordinates where we want to start the checks
        t = max(li - streak + 1, 0)
        b = min(state.M - streak, li)
        l = max(lj - streak + 1, 0)
        r = min(state.N - streak, lj)

        # Check vertical
        for i in range(t, b + 1):
            if all((state.board[i + k][lj] == color for k in range(streak))):
                return True

        # Check horizontal
        for j in range(l, r + 1):
            if all((state.board[li][j + k] == color for k in range(streak))):
                return True

        # Check top left to bottom right diagonal
        for d in range(-min(li - t, lj - l), min(b - li, r - lj) + 1):
            if all((state.board[li + d + k][lj + d + k] == color for k in range(streak))):
                return True

        # Check bottom left to top right diagonal
        for d in range(-min(state.M - 1 - li, lj, streak - 1), min(li - streak + 1, state.N - streak - lj, 0) + 1):
            if all((state.board[li - d - k][lj + d + k] == color for k in range(streak))):
                return True
            
        return False

    def evaluate(self, state, color):
        """Evaluates the state for the player with the given stone color.

        This function calculates the length of the longest ``streak'' on the board
        (of the given stone color) divided by K.  Since the longest streak you can
        achieve is K, the value returned will be in range [1 / state.K, 1.0].

        Args:
            state (State): The state instance for the current board.
            color (int): The color of the stone for which to calculate the streaks.

        Returns:
            the evaluation value (float), from 1.0 / state.K (worst) to 1.0 (win).
        """
        
        ans = 0
        
        for y in range(state.M): # Goes through each board space and checks
            for x in range(state.N): # the heuristic if the space contains
                if(state.board[y][x] == color): # a piece of the given color
                    streak = 1
                    while streak+1 <= state.K and self.has_streak(state, color, streak+1, y, x):
                        streak = streak+1
                    ans = max(ans, streak)
                    
        return float(ans) / float(state.K)

    def min_value(self,state,alpha,beta,display,depth):
        if (state.is_terminal()):
            return (state.utility(self),0)
        
        if (depth >= self.limit):
            return (self.evaluate(state,self.color),0)
        
        if(self.boards_visited_min.has_key(str(state))):
            return (-99.0,0)
        self.boards_visited_min[str(state)] = 1

        v = 99.0
        position = 0
        previous_v = v
        i = 0
        for action in state.actions():
            v = min(v, (self.max_value(state.result(action),alpha,beta,False,depth+1))[0])
            if (v <= alpha):
                return (v,i)

            if (v != previous_v):
                position = i
                previous_v = v
            beta = min(beta,v)
            i += 1
        return (v,position)

    def max_value(self,state,alpha,beta,display,depth):
        if (state.is_terminal()):
            return (state.utility(self),0)
        
        if (depth >= self.limit):
            return (self.evaluate(state,self.color),0)
        
        if(self.boards_visited_max.has_key(str(state))):
            return (99.0,0)
        self.boards_visited_max[str(state)] = 1

        v = -99.0
        position = 0
        previous_v = v
        i = 0
        for action in state.actions():
            v = max(v, (self.min_value(state.result(action),alpha,beta,False,depth+1))[0])
            if (v >= beta):
                return (v,i)

            if (v != previous_v):
                position = i
                previous_v = v
            alpha = max(alpha,v)
            i += 1
        return (v,position)

